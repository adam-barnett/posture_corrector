import cv2
import winsound

video_src = 0
face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
cam = cv2.VideoCapture(video_src)
ticker = 0
beep_counter = 0
face_image = None
cascade_checking = 0
face_found = False

while True:
    ret, img = cam.read()
    if ret:
        img_copy = cv2.resize(img, (img.shape[1]/2, img.shape[0]/2))
        gray = cv2.cvtColor(img_copy, cv2.COLOR_BGR2GRAY)
        if cascade_checking == 0:
            rects = face_cascade.detectMultiScale(gray, 1.3, 5)
            if len(rects) == 0:
                #the face has not been detected
                face_found = False
            else:
                #the face is currently being detected
                face_found = True
                cascade_checking = 20
                for x, y, width, height in rects:
                    cv2.rectangle(img_copy, (x, y), (x+width, y+height),
                                  (255,0,0), 2)
                    face_image = gray[y:y+height, x:x+width]
                    cascade_checking = 20
        else:
            cascade_checking -= 1
            if face_image is None:
                continue
            method = eval('cv2.TM_CCOEFF_NORMED')
            face_match = cv2.matchTemplate(face_image, gray, method)
            _, max_val, _, _ = cv2.minMaxLoc(face_match)
            if max_val > 0.75:
                face_found = True
            else:
                face_found = False
        if face_found:
            if ticker < 10:
                ticker += 1
            if ticker > 5:
                beep_counter = 0
        else:
            if ticker > -20:
                ticker -= 1
            if ticker < -10 and beep_counter < 15:
                winsound.Beep(2500, 400)
                beep_counter += 1
                ticker = 0
        cv2.imshow('facedetect', img_copy)
        if cv2.waitKey(20) == 27:
            #if escape is hit then the program shuts down
            cv2.destroyAllWindows()
            cam.release()
            break



